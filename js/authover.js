(function($) {

	var root = this;

	var iFrameAttribute, timeOut;
	var modalContent = "";
	var width = Number.NaN;
	var height = Number.NaN;
	var delay = Number.NaN;
	var log = [];
	var SENS = 20;
	var hasFired = 0;
	var enableModal = false;
	var modalISActive = false;
	var maxModalFires = 1;
	var popupUseAjax = false;
	var targetUseIframe = false;
	var dialog,
		dialogOpened = false;

	function start() {
		//alert('Starting');
		if (targetUseIframe) {
			initIframe();
		}
		if (modalContent != '') {
			//alert("using modal content " + modalContent);
			initModal();
		}
		// initFrameBuster();
	}

	function initIframe() {
		$('<iframe/>', iFrameAttribute).appendTo('body');

	}

	function initModal() {
		// ------------- add popup ------------------------
		var pop = '';
		if (modalContent != null && modalContent.indexOf("http://") == 0) {
			//alert("using linked content");
			if (popupUseAjax ) {
				//alert("using iframe ajax content");
				// use ajax
				//TODO handle this

				pop = '<div style="display: none;"><a id="btnForm" href="'
					+ modalContent + '"></a></div>';

			} else {
				//alert("using iframe remote content");
				// use iframe
				/*
				pop = '<div style="display: none;"><a id="btnForm" href="'
						+ modalContent + '" class="various iframe"></a></div>';
				*/
				$('<iframe id="exit-content" src="' + modalContent + '" class="iframe-load"></iframe>').appendTo('body');

                $('#exit-content').load(function() {
                    $(this).modal(
                        {
                            onOpen: function(dl) {
                                dialog = dl;
                            },
                            onClose: modalClose
                        }
                    );

                  if (width && height) {
                    $('#simplemodal-container').width(width).height(height);
                  }

                  $(this).removeClass('iframe-load');

                });
			}

		} else {
			//alert("using inline content");
			/*
			pop = '<div style="display: none;"><a id="btnForm" href="#inline1"></a><div id="inline1">'
					+ modalContent + '</div></div>';
			*/
			$('<div id="exit-content"></div>').append(modalContent).appendTo('body');
		}
		//$(pop).appendTo('body');
		// ------------- add events to trigger popup when appropriate------------------------
		
		// Used in iframe mode //
		$('#authFrame').mouseenter(enableIframeOut);
		$('#authFrame').mouseleave(triggerIframeOut);
		////////////////////////////////////////////////////// 
		// used in fetch+injection mode, as Iframes can't see mousemovement over in cross domain
		$(document).mousemove(function(e) {
			enableModal = true;
			var ct = new Date().getTime();
			if (log.length > 1) {
				var o = log[log.length - 1];
				var dt = ct - o.time;
				// var nx = e.pageX + (((e.pageX - o.x) / dt) * SENS);
				var ny = e.pageY + (((e.pageY - o.y) / dt) * SENS); // predictive
				// based on
				// historical
				// velocity
				if (ny < 5) {
					// if (e.pageY <= 5) { //oldschool static
					triggerModal();
				}
			}
			log.push({
				x : e.pageX,
				y : e.pageY,
				time : ct
			});

		});
		//---------------- delay --------------------------
		if (!isNaN(delay)) {
			if (delay > 0) {
			//	alert("delay > 0");
				timeOut = setTimeout(triggerModal, 1000 * delay);
			} else if (delay == 0) {
				//alert("delay == 0");
				// need a small delay to let the page load
				timeOut = setTimeout(triggerModal, 150);
				// triggerModal();
				// $.fancybox.showActivity();
			}
		}
	}
	function enableIframeOut() {
		// alert('mouseout');
		enableModal = true;
	}
	function triggerIframeOut() {
		// alert('mouseout');
		if (enableModal) {
			triggerModal();
		}
	}
	function triggerModal() {
		// alert('triggerModal');

		clearTimeout(timeOut);

        if (!dialog) {
            $('#exit-content').removeClass('iframe-load').modal(
                {
                    onOpen: function(dl) {
                        dialog = dl;
                    },
                    onClose: modalClose
                }
            );

            if (width && height) {
              $('#simplemodal-container').width(width).height(height);
            }
        }

		if (!dialogOpened && hasFired < maxModalFires) {
			dialogOpened = true;
			modalOpen(dialog);
		}

		/*
		if (!modalISActive && hasFired < maxModalFires) {
			// alert('triggerModal ' + width + " " + height);

			if (isNaN(width) || isNaN(height)) {
				// alert("autoszing");
				// for embedded mode, when not explicitly passed in.
				$('a#btnForm').fancybox({
					autoDimensions : true,
					onComplete : onModalOpened,
					onCleanup : onModalClosed
				}).trigger('click');
			} else {
				//alert("explicit height/width");
				// for iframe mode, when height/width explicitly passed in
				$('a#btnForm').fancybox({
					autoDimensions : false,
					height : height,
					width : width,
					onComplete : onModalOpened,
					onCleanup : onModalClosed
				}).trigger('click');
			}
		}
		*/
	}

	/**
	 * When the open event is called, this function will be used to 'open'
	 * the overlay, container and data portions of the modal dialog.
	 *
	 * onOpen callbacks need to handle 'opening' the overlay, container
	 * and data.
	 */
	function modalOpen(dialog) {
		dialog.overlay.fadeIn('fast', function () {
			dialog.container.fadeIn('fast', function () {
				dialog.data.hide().slideDown('fast');
			});
		});
	}

	/**
	 * When the close event is called, this function will be used to 'close'
	 * the overlay, container and data portions of the modal dialog.
	 *
	 * The SimpleModal close function will still perform some actions that
	 * don't need to be handled here.
	 *
	 * onClose callbacks need to handle 'closing' the overlay, container
	 * and data.
	 */
	function modalClose(dialog) {
		dialogOpened = false;
		hasFired++;
		dialog.data.fadeOut('fast', function () {
			dialog.container.hide('fast', function () {
				dialog.overlay.slideUp('fast', function () {
					$.modal.close();
				});
			});
		});
	}


	function initFrameBuster() {
		new authover.FrameBuster({
		// url : 'http://www.url.com/' // TODO change this
		});
	}
	/*
	function onModalOpened() {
		modalISActive = true;
	}

	function onModalClosed() {
		hasFired++;
		modalISActive = false;
	}
	*/

	// expose public api

	root.authover = root.authover || {};

	root.authover.configure = function(p) {
		// alert("p " + p);
		/*if(false){
		for(var k in p){
			alert("k " +k + " "+ p[k] + " " + root[k]);
			//this[k] = p[k];
		}}*/
		if (typeof p.iFrameAttribute != "undefined"  &&  p.iFrameAttribute !=''  &&  p.iFrameAttribute !=null) {
			iFrameAttribute = p.iFrameAttribute;
		}
		
		if (typeof p.targetUseIframe != "undefined" &&  p.targetUseIframe !='' &&  p.targetUseIframe !=null) {
			if(p.targetUseIframe == '0'){
				targetUseIframe = false;
			}else if(p.targetUseIframe == '1'){
				targetUseIframe = true;
			}else{
				targetUseIframe = p.targetUseIframe;
			}

		}
		//popup content------------------------------------
		if (typeof p.modalContent != "undefined" &&  p.modalContent !='' &&  p.modalContent !=null) {
			modalContent = p.modalContent;
		}
		if (typeof p.width != "undefined" &&  p.width !='' &&  p.width !=null) {
			// alert('has width ' + p.width);
			width = Number(p.width);
		}
		if (typeof p.height != "undefined" &&  p.height !='' &&  p.height !=null) {
			 //alert('has height ' + p.height);
			height = Number(p.height);
		}
		if (typeof p.delay != "undefined" &&  p.delay !=''&&  p.delay !=null) {
		//	 alert('has delay ' + p.delay);
			delay = Number(p.delay);
		}
		
		if (typeof p.popupUseAjax != "undefined" &&  p.popupUseAjax !=''&&  p.popupUseAjax !=null) {
			if(p.popupUseAjax == '0'){
				popupUseAjax = false;
			}else if(p.popupUseAjax == '1'){
				popupUseAjax = true;
			}else{
				popupUseAjax = p.popupUseAjax;
			}
			
		}

		// frameborder =p.frameborder;
		start();
	};

}).call(this, jQuery);