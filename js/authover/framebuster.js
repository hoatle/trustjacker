/**
 * The FrameBuster module.
 *
 * See more: http://en.wikipedia.org/wiki/Framekiller
 *
 * Usage:
 *
 * var frameBuster = new authover.FrameBuster({
 *   url: 'http://server-which-responds-with-204.example.com/'
 * });
 *
 */

(function($) {

  var root = this;

  /**
   * The FrameBuster class.
   *
   * @param options
   *        + url
   */
  var FrameBuster = function(options) {
    this.url = options.url;

    init.call(this);

  };

  function init() {

    var preventBust = 0;


    $(window).on('beforeunload', function(e) {
      preventBust++;
    });


    // Event handler to catch execution of the busting script.
    //window.onbeforeunload = function() { preventBust++ };

    setInterval($.proxy(function() {

      if (preventBust > 0) {
        // Yes: it has fired.
        // Avoid further action.
        preventBust -= 2;
        // Get a ‘No Content’ status which keeps us on the same page.
        window.top.location = this.url;
      }
    }, this), 1);

  }


  root.authover = root.authover || {};

  root.authover.FrameBuster = FrameBuster;

}).call(this, jQuery);