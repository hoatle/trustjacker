/**
 * The popuplist module
 */

(function($, HandleBars) {

  var root = this;

  //configurable stuffs

  var data = [];

  var popupDelayOptions = '{"5":"5","10":"10","15":"15","20":"20","25":"25","30":"30"}';


  var targetMethodOptions = '{"iframe":"IFrame","proxy":"Proxy"}';

  var targetWeightOptions = '{"1":"1","2":"2","3":"3","4":"4","5":"5","6":"6","7":"7","8":"8","9":"9","10":"10"}';

  var popupDelayJsonObj, targetMethodJsonObj, targetWeightJsonObj;

  var popupEntryTemplate, $tableBody;

  function PopupList(params) {
    this.init(params);
  }


  $.extend(PopupList.prototype, {
    init: function(params) {

      data = params.data || data;

      popupDelayOptions = params.popupDelayOptions || popupDelayOptions;

      targetMethodOptions = params.targetMethodOptions || targetMethodOptions;

      targetWeightOptions = params.targetWeightOptions || targetWeightOptions;

      popupDelayJsonObj = $.parseJSON(popupDelayOptions);
      targetMethodJsonObj = $.parseJSON(targetMethodOptions);
      targetWeightJsonObj = $.parseJSON(targetWeightOptions);

      initEntryForm();

      popupEntryTemplate = Handlebars.compile($('#popup-entry-template').html());

      $tableBody = $('#popuplist>tbody');

      initTabularData.call(this);

      $('.line').peity('line');

      $('.editable').editable(function(value, settings) {

        var $el = $(this);
        var entryId = $el.closest('tr').attr('data-id');
        var dataName = $el.attr('data-name');
        var originalValue = $el.attr('data-original');

        if (originalValue !== value) {
          var params = {
            id: entryId,
            originValue: originalValue,
            newValue: value
          };

          if (dataName === 'link') {
            $tableBody.trigger('change-link', [params]);
          } else if (dataName === 'target') {
            $tableBody.trigger('change-target-link', [params]);
          } else if (dataName === 'popup') {
            $tableBody.trigger('change-popup-link', [params]);
          }
        }

        return escapeHTMLTag(value);
      }, {
        indicator : 'Saving...',
        tooltip   : 'Click to edit...',
        data: function(value, settings) {
          return unEscapeHTMLTag(value);
        }
      });

      $('.target-method.selectable').editable(function(value, settings) {

        var $el = $(this),
          entryId = $el.closest('tr').attr('data-id'),
          originalValue = $el.attr('data-original');

        if (originalValue !== value) {
          var params = {
            id: entryId,
            originValue: originalValue,
            newValue: value
          };

          $tableBody.trigger('change-target-method', [params]);
        }

        return value;

      }, {
        type: 'select',
        submit: 'OK',
        data: targetMethodOptions,
        indicator: 'Saving...',
        tooltip: 'Click to edit...',
        style: 'display: inline'
      });

      $('.target-weight.selectable').editable(function(value, settings) {

        var $el = $(this),
          entryId = $el.closest('tr').attr('data-id'),
          originalValue = $el.attr('data-original');

        if (originalValue !== value) {
          var params = {
            id: entryId,
            originValue: originalValue,
            newValue: value
          };

          $tableBody.trigger('change-target-weight', [params]);
        }

        return value;
      }, {
        type: 'select',
        submit: 'OK',
        data: targetWeightOptions,
        indicator: 'Saving...',
        tooltip: 'Click to edit...',
        style: 'display: inline'
      });


      $('.popup-delay.selectable').editable(function(value, settings) {

        var $el = $(this),
          entryId = $el.closest('tr').attr('data-id'),
          originalValue = $el.attr('data-original');

        if (originalValue !== value) {
          var params = {
            id: entryId,
            originValue: originalValue,
            newValue: value
          };

          $tableBody.trigger('change-popup-delay', [params]);
        }

        return value;
      }, {
        type: 'select',
        submit: 'OK',
        data: popupDelayOptions,
        indicator: 'Saving...',
        tooltip: 'Click to edit...',
        style: 'display: inline'
      });


      $('#popuplist').dataTable({
        "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
          "sLengthMenu": "_MENU_ records per page"
        }
      });
    }

  });

  function initTabularData() {
    for (var i = 0; i < data.length; i++) {
      var entry = popupEntryTemplate(data[i]);

      $tableBody.append(entry);

    }

    $('.btn-delete-name').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('delete-name', [{
        id: getId($(this))
      }]);
    });

    $('.btn-reset-total-hits').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('reset-total-hits', [{
        id: getId($(this))
      }]);

    });

    $('.btn-reset-total-unique').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('reset-total-unique', [{
        id: getId($(this))
      }]);

    });

    $('.btn-open-link').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('open-link', [{
        id: getId($(this))
      }]);
    });

    $('.btn-tweet-link').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('tweet-link', [{
        id: getId($(this))
      }]);

    });

    $('.btn-email-link').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('email-link', [{
        id: getId($(this))
      }]);
    });

    $('.btn-delete-link').on('click', function(e) {
      noDefaultAndPropagation(e);

      $tableBody.trigger('delete-link', [{
        id: getId($(this))
      }]);

    });

    $('.btn-delete-target').on('click', function(e) {
      noDefaultAndPropagation(e);

      var $el = $(this);

      $tableBody.trigger('delete-target', [{
        id: getId($el),
        link: getLink($el)
      }]);
    });

    $('.btn-delete-popup').on('click', function(e) {
      noDefaultAndPropagation(e);

      var $el = $(this);

      $tableBody.trigger('delete-popup', [{
        id: getId($el),
        link: getLink($el)
      }]);

    });



    function noDefaultAndPropagation(e) {
      e.preventDefault();
      e.stopPropagation();
    }


    function getId($el) {
      return $el.closest('tr').attr('data-id');
    }

    function getLink($el) {
      return $el.closest('div').find('.editable').html();
    }


    registerEventHandlers();

  }


  function registerEventHandlers() {


    $tableBody.find('tr').on('click', function(e) {


      var $tdEntry = $(this).find('td').first();

      $('.entry.selected').not($tdEntry).find('.actions').addClass('hide');
      $('.entry.selected').not($tdEntry).removeClass('selected');

      $tdEntry.toggleClass('selected');
      $(this).find('.btn-delete-name').toggleClass('hide');

    });

    $tableBody.find('.entry').on('click', function(e) {
      e.stopPropagation();

      //work-around with jediable plugin
      if ($(this).find('form').length > 0) {
        return;
      }


      $('.entry.selected').not(this).find('.actions').addClass('hide');
      $('.entry.selected').not(this).removeClass('selected');

      $(this).toggleClass('selected');
      $(this).find('.actions').toggleClass('hide');
    });


    $tableBody.on('delete-name', function(e, params) {

      authover.dialog({
        type: 'confirm',
        level: 'danger',
        msg: {
          header: 'Are you sure to delete this entry?',
          body: 'Name: <span class="highlight">' + getPopupEntryById(params.id).name + '</span>',
          no: 'No, I do not want to delete this entry',
          yes: 'Yes, I confirm to delete this entry'
        },
        yes: function($el) {
          $.log('TODO implement delete name');

          $el.modal('hide');
        },
        no: function($el) {
          $el.modal('hide');
        }
      });

    });

    $tableBody.on('reset-total-hits', function(e, params) {

      authover.dialog({
        type: 'confirm',
        level: 'danger',
        msg: {
          header: 'Are you sure to reset this total hits?',
          body: 'Current total hits: <span class="highlight">' + getPopupEntryById(params.id).hits.total + '</span>',
          no: 'No, I do not want to reset this total hits',
          yes: 'Yes, I confirm to reset this total hits'
        },
        yes: function($el) {
          $.log('TODO implement reset total hits');

          $el.modal('hide');
        },
        no: function($el) {
          $el.modal('hide');
        }
      });

    });

    $tableBody.on('reset-total-unique', function(e, params) {

      authover.dialog({
        type: 'confirm',
        level: 'danger',
        msg: {
          header: 'Are you sure to reset this total unique?',
          body: 'Current total unique: <span class="highlight">' + getPopupEntryById(params.id).unique.total + '</span>',
          no: 'No, I do not want to reset this total unique',
          yes: 'Yes, I confirm to reset this total unique'
        },
        yes: function($el) {
          $.log('TODO implement reset total unique');

          $el.modal('hide');
        },
        no: function($el) {
          $el.modal('hide');
        }
      });

    });

    $tableBody.on('open-link', function(e, params) {
      $.log('open-link', params);
    });

    $tableBody.on('tweet-link', function(e, params) {
      $.log('tweet-link', params);
    });

    $tableBody.on('email-link', function(e, params) {
      $.log('email-link', params);
    });

    $tableBody.on('delete-link', function(e, params) {

      authover.dialog({
        type: 'confirm',
        level: 'danger',
        msg: {
          header: 'Are you sure to delete this link?',
          body: 'Link: <span class="highlight">' + getPopupEntryById(params.id).link + '</span>',
          no: 'No, I do not want to delete this link',
          yes: 'Yes, I confirm to delete this link'
        },
        yes: function($el) {
          $.log('TODO implement delete link');

          $el.modal('hide');
        },
        no: function($el) {
          $el.modal('hide');
        }
      });

    });


    $tableBody.on('delete-target', function(e, params) {

      authover.dialog({
        type: 'confirm',
        level: 'danger',
        msg: {
          header: 'Are you sure to delete this target?',
          body: 'Target link: <span class="highlight">' + params.link + '</span>',
          no: 'No, I do not want to delete this target',
          yes: 'Yes, I confirm to delete this target'
        },
        yes: function($el) {
          $.log('TODO implement delete target');

          $el.modal('hide');
        },
        no: function($el) {
          $el.modal('hide');
        }
      });
    });


    $tableBody.on('delete-popup', function(e, params) {

      authover.dialog({
        type: 'confirm',
        level: 'danger',
        msg: {
          header: 'Are you sure to delete this popup?',
          body: 'Popup link: <span class="highlight">' + params.link + '</span>',
          no: 'No, I do not want to delete this popup',
          yes: 'Yes, I confirm to delete this popup'
        },
        yes: function($el) {
          $.log('TODO implement delete popup');

          $el.modal('hide');
        },
        no: function($el) {
          $el.modal('hide');
        }
      });
    });

    $tableBody.on('change-link', function(e, params) {
      $.log('change-link', params);
    });

    $tableBody.on('change-target-link', function(e, params) {
      $.log('change-target-link', params);
    });

    $tableBody.on('change-popup-link', function(e, params) {
      $.log('change-popup-link', params);
    });

    $tableBody.on('change-target-method', function(e, params) {
      $.log('change-target-method', params);
    });

    $tableBody.on('change-target-weight', function(e, params) {
      $.log('change-target-weight', params);
    });

    $tableBody.on('change-popup-delay', function(e, params) {
      $.log('change-popup-delay', params);
    });

  }

  function initEntryForm() {

    var methods = getArrayOfKeyValueObject(targetMethodJsonObj),
      weights = getArrayOfKeyValueObject(targetWeightJsonObj),
      delays = getArrayOfKeyValueObject(popupDelayJsonObj);

    var formTargetTemplate = HandleBars.compile($('#form-target-template').html()),
      formPopupTemplate = HandleBars.compile($('#form-popup-template').html());


    var $newEntryForm = $('#new-entry-form'),
      targetCounter = 0, popupCounter = 0;

    //addTarget();
    //addPopup();

    var $name = $newEntryForm.find('#name'),
      $link = $newEntryForm.find('#link');

    $name.on('blur', function(e) {
      e.preventDefault();
      isValidName();
    });

    $link.on('blur', function(e) {
      e.preventDefault();
      isValidLink();
    });

    $('#add-more-target-btn').on('click', function(e) {
      e.preventDefault();
      addTarget();
      targetCounter++;
    });

    $('#add-more-popup-btn').on('click', function(e) {
      e.preventDefault();
      addPopup();
      popupCounter++;
    });


    $('#submit-entry-btn').on('click', function(e) {
      e.preventDefault();

      if (isValidEntryForm()) {
        $.log('submit entry form');

        $.log('entryData', getEntryData());

        $newEntryForm.modal('hide');
      }

    });

    $('#cancel-entry-link').on('click', function(e) {
      e.preventDefault();

      $newEntryForm.modal('hide');

    });


    $('#add-new-entry-btn').on('click', function(e) {
      e.preventDefault();
      $newEntryForm.modal();
    });



    function addTarget() {
      var $formTarget = $(formTargetTemplate({
        id:targetCounter,
        methods: methods,
        weights: weights
      })).hide();
      $newEntryForm.find('.targets-container').append($formTarget);
      $formTarget.fadeIn('slow');
      $('#delete-target-btn-'+targetCounter).on('click', function(e) {
        e.preventDefault();
        var $entry = $(this).closest('div.entry');
        $entry.fadeOut('slow', function(e) {
          $(this).remove();
        });
      });
    }

    function addPopup() {
      var $formPopup = $(formPopupTemplate({
        id:popupCounter,
        delays: delays
      })).hide();
      $newEntryForm.find('.popups-container').append($formPopup);
      $formPopup.fadeIn('slow');

      $('#delete-popup-btn-'+popupCounter).on('click', function(e) {
        e.preventDefault();
        var $entry = $(this).closest('div.entry');
        $entry.fadeOut('slow', function(e) {
          $(this).remove();
        });
      });
    }

    function isValidEntryForm() {
      var validName = isValidName(),
        validLink = isValidLink();
      return validName && validLink;
    }

    function isValidName() {
      if ($name.val().length < 1) {
        $name.closest('div.control-group').addClass('error');
        return false;
      } else {
        $name.closest('div.control-group').removeClass('error');
        return true;
      }
    }

    function isValidLink() {
      if ($link.val().length < 1) {
        $link.closest('div.control-group').addClass('error');
        return false;
      } else {
        $link.closest('div.control-group').removeClass('error');
        return true;
      }
    }

    /**
     * Gets the json entry data
     */
    function getEntryData() {
      var entryData = {};

      entryData.name = $name.val();
      entryData.link = $link.val();

      var targets = getTargetsData();
      if (targets.length > 0) {
        entryData.targets = targets;
      }

      var popups = getPopupsData();
      if (popups.length > 0) {
        entryData.popups = popups;
      }

      return entryData;
    }

    function getTargetsData() {
      var targets = [];
      for (var i = 0; i < targetCounter; i++) {
        var $targetLink = $('#target-link-' + i),
          $targetMethod = $('#target-method-' + i),
          $targetWeight = $('#target-weight-' + i);
        if ($targetLink && $targetMethod && $targetWeight &&
          $targetLink.val().length > 0) {
          targets.push({
            link: $targetLink.val(),
            method: $targetMethod.val(),
            weight: $targetWeight.val()
          });
        }
      }
      return targets;
    }

    function getPopupsData() {
      var popups = [];

      for (var i = 0; i < popupCounter; i++) {
        var $popupLink = $('#popup-link-' + i),
          $popupDelay = $('#popup-delay-' + i);

        if ($popupLink && $popupDelay && $popupLink.val().length > 0) {
          popups.push({
            link: $popupLink.val(),
            delay: $popupDelay.val()
          });
        }

      }

      return popups;
    }


   function getArrayOfKeyValueObject(jsonObject) {
     var results = [];
     $.map(jsonObject, function(value, key) {
      results.push({
        key: value,
        value: key
      });
     });

     return results;
   }


  }

  function escapeHTMLTag(text) {
    return text.replace(/</g,'&lt;').replace(/>/g, '&gt;');
  }

  function unEscapeHTMLTag(text) {
    return text.replace(/&lt;/g,'<').replace(/&gt;/g, '>');
  }


  function getPopupEntryById(id) {
    var found = null;
    for (var i = 0, len = data.length; i < len; i++) {
      if (data[i].id === id) {
        found = data[i];
        break;
      }
    }
    return found;
  }


  root.authover = root.authover || {};

  root.authover.popupList = function(params) {
    return new PopupList(params);
  };


}).call(this, jQuery, Handlebars, authover);