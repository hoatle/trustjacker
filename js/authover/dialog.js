/**
 * The dialog component leverages bootstrap modal dialog to use instead of alert/confirm from browser.
 *
 * Usage:
 *
 * authover.dialog({
 *   type: '', //one of alert, confirm
 *   level: '', //one of warning, danger
 *   msg: {
 *     header: '',
 *     body: '',
 *     yes: '',
 *     no: ''
 *   },
 *   yes: function() {
 *
 *   },
 *   no: function() {
 *
 *   },
 *
 *   hide: function() {
 *
 *   },
 *   autoClose: 5000 //ms
 * });
 *
 */

(function($, Handlebars) {

  var root = this;

  var templateHtml = [];
  templateHtml.push('<div class="modal fade hide" id="authover-modal">');
    templateHtml.push('<div class="modal-header">');
      templateHtml.push('<button type="button" class="close" data-dismiss="modal">x</button>');
      templateHtml.push('<h3>{{msg.header}}</h3>');
    templateHtml.push('</div>');
    templateHtml.push('<div class="modal-body">');
      templateHtml.push('<p>{{{msg.body}}}</p>');
    templateHtml.push('</div>');
   templateHtml.push('<div class="modal-footer">');
    templateHtml.push('{{#if confirmType}}');
    templateHtml.push('<a id="btn-no" href="#" class="btn">{{msg.no}}</a>');
    templateHtml.push('{{/if}}');
    templateHtml.push('<a id="btn-yes" href="#" class="btn btn-primary btn-{{level}}">{{msg.yes}}</a>');
   templateHtml.push('</div>');
  templateHtml.push('</div>');

  templateHtml = templateHtml.join('');

  var modalTemplate = Handlebars.compile(templateHtml);


  var defaultParams = {
    type: 'alert',
    level: 'warning',
    msg: {
      header: 'Alert',
      body: 'Default Message...',
      yes: 'OK',
      no: 'Cancel'
    },
    yes: function() {},
    no: function() {},
    hide: function() {},
    autoClose: 0 //does not auto close
  };

  function dialog(params) {
    $('#authover-modal').remove();

    var params = $.extend({}, defaultParams, params);

    init(params);
  }

  function init(params) {

    if (params.type === 'confirm') {
      params.confirmType = true;
    }

    $('body').append(modalTemplate(params));

    var $authoverModal = $('#authover-modal');

    $authoverModal.modal();

    var yesBtn = $authoverModal.find('#btn-yes'),
      noBtn = $authoverModal.find('#btn-no');

    yesBtn.on('click', function(e) {
      e.preventDefault();

      params.yes($authoverModal);

    });

    noBtn.on('click', function(e) {
      e.preventDefault();

      params.no($authoverModal);
    });

    $authoverModal.on('hide', function() {
      params.hide($authoverModal);
    });

  }

  //export

  root.authover = root.authover || {};

  root.authover.dialog = dialog;

}).call(this, jQuery, Handlebars);